.. Python SMail documentation master file, created by
   sphinx-quickstart on Sat Mar 28 19:43:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python SMail's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   smail

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
